#! /usr/bin/python3
# -*- coding: utf-8 -*-

# Orejon es un programa que comprime TODOS los archivos de audio que tengas
# en un directorio y los subdirectorios que cuelguen de él.
# El objeto de Orejon es optimizar el espacio de unidades de memoria que
# habitualmente se utilizan para llevar música contigo (llaveros usb,
# reproductores de mp3, etc.).

import argparse
from pathlib import PurePath
from subprocess import Popen, PIPE
from funciones_nombres_orejon import extension
from funciones_ffmpeg_orejon import bit_rate, comprime


# AQUÍ LOS VALORES QUE SE ENTREGAN AL PROGRAMA COMO ARGUMENTOS:

# Iniciar una lista de argumentos para los argumentos.
lista_argumentos = argparse.ArgumentParser(description=
    "Comprime el audio que haya en el directorio que se indique")

# Aquí se indica si, una vez comprimido, se quiere borrar el archivo original.
lista_argumentos.add_argument("-b", "--borrar", action="store_true",
    default=False, help="borra los archivos originales tras comprimirlos")

# En caso de compresión, este será el tipo de archivo preferido del archivo de
# salida.
lista_argumentos.add_argument("-t", "--tipo", type=str, choices=["ogg", "mp3"],
    default='ogg', help="tipo de compresión por defecto, ogg o mp3")

# El bit rate preferido. Expresado en Kbit/s.
lista_argumentos.add_argument("-r", "--bitrate", type=int,
    choices=[128, 160, 192, 256, 320], default=192,
    help="bit rate, en Kbit/s, al que se comprimirán los archivos de audio")

# Esta es la ruta del directorio raíz sobre el que se hará la búsqueda de los
# archivos de audio que se van a comprimir, si cumplen los requisitos.
lista_argumentos.add_argument("directorio",
    help="directorio donde se buscarán los archivos de audio a comprimir")

# Si la ruta se indica en forma relativa, el script de lanzamiento de Orejon
# captura la raíz de la ruta y la entrega a Orejon.
lista_argumentos.add_argument("raiz")


# CARGA DE LAS VARIABLES DEL PROGRAMA Y MOSTRAR AL USUARIO PARA CONFIRMACIÓN

# Inicializamos el uso de argumentos
argumento = lista_argumentos.parse_args()

# Determinación del directorio de trabajo. Con los métodos de pathlib determino
# si se entregó el directorio en forma absoluta o no. Si se entregó en forma
# absoluta, la ruta es igual al argumento directorio. Si no se entregó en
# forma absoluta, se compone la ruta con el argumento del directorio relativo
# y la raiz que captura el script de lanzamiento.
raiz = argumento.raiz
directorio = argumento.directorio
if PurePath(directorio).is_absolute():
    ruta = str(directorio)
else:
    ruta = str(PurePath(raiz).joinpath(directorio))

# Carga del bit rate.
bitrate = argumento.bitrate

# Carga del tipo de compresión por defecto
extension_salida = argumento.tipo

# Determinar si se borran los archivos originales
borrar = argumento.borrar

# Hacer una lista de los archivos de audio que Orejón reconoce como tales.
# La lista de extensiones de archivos de audio la saco de un archivo de
# texto que guardo en /etc/orejon/ojeron.list, por lo tanto, lo primero
# que hago es abrir ese archivo y crear la lista de extensiones
extensiones = []
listado_extensiones_orejon = open('/etc/orejon/orejon.list', 'r')
for linea in listado_extensiones_orejon:
    # El archivo /etc/orejon/ojeron.list admite comentarios, los ignoro
    if linea[0] == "#" or linea == "\n":
        pass
    else:
    # Al leer un elemento de la lista, se carga el salto de línea "\n", con
    # la opción [:-1] elimino el salto de línea.
        extensiones.append(linea[:-1])
# Cierro el archivo
listado_extensiones_orejon.close()

# Mostrar por pantalla y pedir confirmación
print("")  # línea en blanco por motivos de legibilidad.
print("Orejón buscará todos los archivos de audio cuyo ratio de",
    "compresión sea mayor de", bitrate, "Kbit/s y que cuelguen del directorio",
    "(y sus subdirectorios):")
print("")  # línea en blanco por motivos de legibilidad.
print(ruta)
print("")  # línea en blanco por motivos de legibilidad.
print("Estos archivos se comprimirán en formato", extension_salida,
    "con un ratio de", bitrate, "Kbit/s")
if borrar:
    print("")  # línea en blanco por motivos de legibilidad.
    print("Una vez comprimidos, los archivos originales serán borrados.")
else:
    print("")  # línea en blanco por motivos de legibilidad.
    print("Una vez comprimidos, los archivos originales no serán borrados.")
print("")  # línea en blanco por motivos de legibilidad.
confirmacion = input("¿Continuar? [s/N] ")
if confirmacion == 'S' or confirmacion == 's':
    salir = False
else:
    salir = True


# PASO 0: CONSEGUIR LA LISTA DE ARCHIVOS DEL DIRECTORIO A TRATAR.
# Lanzo la consulta al SO sobre el contenido del directorio. La consulta la
# realizo con el comando "find 'ruta' -type f". "find" busca el archivo que se
# le indique. Al no indicar ningún archivo concreto, si no un directorio,
# "find" busca todos los archivos del directorio. Le añado la opción "-type f"
# para que sólo me relacione los nombres de los archivos encontrados, y
# excluya el nombre de los subdiretorios.
# La ejecución de este comando del SO se lo encargo al módulo subprocess.
# Utilizo el subobjeto Popen y el método PIPE para cargar la salida (stdout)
# del "find" en un fichero. Ojo, que Popen crea por defecto un archivo binario,
# no de texto. Para que capture la información como texto hay que indicarle
# las opciones "bufsize=1, universal_newlines=True". Una vez capturado el
# texto, tengo que limpiar el retorno de carro "\n" que hay al final de cada
# línea.
lista_archivos = Popen(['find', ruta, '-type', 'f'], stdout=PIPE, bufsize=1,
    universal_newlines=True)

# Leo secuencialmente el fichero con la lista de archivos, línea a línea, es
# decir, archivo a archivo.
for archivo in lista_archivos.stdout:

# Si el usuario se raja, salimos
    if salir:
        break
    else:
        pass

# Ahora, proceso la info que hay en el fichero de texto que crea Popen:
# Convierto en string cada una de las entradas del fichero y les quito el fin
# de línea "\n", que lo quito en el mismo acto de convertir en string, con
# la opción [:-1] que quita el último caracter.
    archivo = str(archivo[:-1])

# En este punto ya tengo un string con el nombre y la ruta completa del archivo
# Empiezo con el algoritmo principal del programa:

# PASO 1: DETERMINAR SI SE TRATA DE UN ARCHIVO DE AUDIO
# Leo la extensión del archivo y la comparo con una lista de extensiones de
# ficheros de audio. Por defecto, supongo que no es de audio, para no liarla.
# También verifico que el archivo tenga extensión, por si acaso no la tuviese.
    archivo_audio = False
    (tiene_extension, extension_archivo) = extension(archivo)

# En caso que el archivo tenga extensión, compruebo que está en la lista de
# extensiones de audio que admite Orejón, en cuyo caso pongo en True la
# variable que me identifica que el archivo es de audio.
    if tiene_extension:
        # Procedo con la comprobación de si coincide la extensión del archivo
        # con alguno de la lista de extensiones de audio que reconoce Orejón.
        for extension_audio in extensiones:
            if extension_archivo == extension_audio:
                archivo_audio = True
                break
            else:
                pass
    # Si no tiene extensión no hago nada, ya que ya tengo puesto a False la
    # variable que identifica como fichero de audio al archivo.
    else:
        pass

# PASO 2: DETERMINAR SI HAY QUE COMPRIMIR EL ARCHIVO DE AUDIO
# Si el archivo es de audio, compruebo su bit rate y lo comparo con el bit rate
# máximo que quiero para los archivos de audio del directorio a comprimir.
    if archivo_audio:
        if bit_rate(archivo) > bitrate:
            comprimir = True
        else:
            comprimir = False
    else:
        pass

# PASO 3: COMPRIMIR Y BORRAR EL ARCHIVO, SI PROCEDE
# En este punto sé si el archivo es de audio (archivo_audio = True) y si hay
# que comprimirlo (comprimir = True). También tengo el tipo de archivo
# preferido y la orden de borrado o no del archivo original (se cogieron como
# argumentos de Orejón al invocar el programa).
    if archivo_audio and comprimir:
        print(archivo, "cumple los requisitos indicados.")
        if borrar:
            print("Se comprimirá a", bitrate, "Kbit/s y después será borrado.")
        else:
            print("Se comprimirá a", bitrate, "Kbit/s pero no se borrará")
        print("Trabajando...")
        comprime(archivo, bitrate, extension_salida, borrar)
        print("Tarea terminada.")
    else:
        pass

    if archivo_audio and not(comprimir):
        print(archivo, "no cumple los requisitos indicados.")
        print("Su bit rate es inferior o igual a ", bitrate, "Kbit/s.")
        print("No se realiza ninguna operación.")
    else:
        pass

    if not(archivo_audio):
        print(archivo, "no se identifica como un archivo de audio")
        print("Orejón ignora este archivo")

# Cierro archivos
lista_archivos.stdout.close()

print("")  # línea en blanco por motivos de legibilidad.
print("Trabajo terminado.")