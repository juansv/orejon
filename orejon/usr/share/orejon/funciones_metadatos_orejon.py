#! /usr/bin/python3
# -*- coding: utf-8 -*-

# Módulos: Utilizo el módulo Mutagen, que lee y escribe los metadatos de los
# archivos de audio. Trata de manera distinta los mp3 y los ogg y flac, ya
# que tienen un sistema de etiquetas para metadatos diferentes. Los mp3
# utilizan el estándar ID3 y los ogg y flac no están sujetos a una estructura
# de datos determinada, aunque el estándar de Vorbis recomienda usar siempre
# las mismas etiquetas y define una lista de ellas.
# Inicialmente había previsto la manipulación "artesanal" de los campos
# estándar de ID3, pero resulta un código bastante complejo. Como sólo quería
# usar un puñado concreto y limitado de campos (grupo, album, número de
# canción, año del disco y título de la canción), opté por usar la herramienta
# EasyID3 que incluye el propio mutagen y que simplifica mucho el código. Si
# se quiere manipular el estándar ID3 "a mano", hay que importar los métodos
# de la import que está comentada:
#from mutagen.id3 import TALB, TDAT, TIT1, TIT2, TRCK, ID3NoHeaderError
from mutagen.mp3 import MP3
from mutagen.easyid3 import EasyID3
from mutagen import File
from pathlib import PurePath
from funciones_nombres_orejon import extension, quita_extension

# Funciones que manejan los metadatos de los archivos


###############################################################################
######### Función para escribir un metadato en un archivo de audio ############
###############################################################################

# Esta función recibe el nombre de un archivo de audio, un metadato a escribir
# en el archivo (metadato = nombre del campo + contenido del campo) y la
# opción de sobreescribir el metadato si el archivo de audio ya tuviese ese
# metadato "ocupado". Si no hubiese el metadato o hay orden de sobreescribirlo
# la función escribe el metadato en el archivo de audio. De lo contrario, no
# hace nada. Por lo tanto, esta función no devuelve ningún valor.
# Esta función sólo trabaja con los estándares de metadatos ID3 (para archivos
# de audio mp3) y vorbis (para los ogg y los flac). No se tratan ni los
# formatos de audio de Microsoft ni los de Apple.
# Hablando de Apple, cuando se crean archivos mp3 con "itunes", este programa
# no respeta el estándar ID3, por lo que puede dar problemas al manipular los
# metadatos con Orejón.
def escribe_metadato(metadato, contenido, sobreescribir, archivo):

# A la función hay que entregarle un nombre genérico para el metadato, que
# luego hay que traducir para el caso de las etiquetas id3v2 y vorbis. Para
# estos dos tipos de etiquetas, preparo sendos diccionarios que permitan
# hacer la traducción del nombre genérico que recibe la función. Si se usa
# EasyID3, el diccionario es casi idéntico, con la diferencia que EasyID3 usa
# siempre minúsculas y la recomendación de vorbis es tanto con mayúsculas como
# con minúsculas.
# Inicialmente, había previsto manipular "a mano" las etiquetas del estándar
# ID3v2, pero luego lo desestimé por EasyID3. En todo caso, conservo cómo sería
# el diccionario para manipular "a mano" las etiquetas ID3v2:
#    id3v2 = {'DISCO': 'TALB', 'FECHA': 'TDAT', 'GRUPO': 'TIT1',
#            'TITULO': 'TIT2', 'ORDEN': 'TRCK'}
    id3v2 = {'DISCO': 'album', 'FECHA': 'date', 'GRUPO': 'artist',
            'TITULO': 'title', 'ORDEN': 'tracknumber'}
    vorbis = {'DISCO': 'ALBUM', 'FECHA': 'DATE', 'GRUPO': 'ARTIST',
            'TITULO': 'TITLE', 'ORDEN': 'TRACKNUMBER'}

# También necesitamos saber la extensión del archivo de audio:
    (tiene_extension, ext) = extension(archivo)

# Dependiendo del tipo de archivo, cargamos los metadatos que tiene.
# Si es un mp3, probamos si el archivo tiene los metadatos estructurados con
# el estándar ID3. Para ello usamos el método MP3 de mutagen, indicando que
# cargue las etiquetas ID3 según la herramienta EasyID3, que simplifica las
# etiquetas.
# Si el archivo de audio no tuviese los datos conforme al estándar ID3 (caso
# de ogg y flac), cargamos los metadatos con el método File del módulo
# mutagen.
# Cualquiera de los dos métodos (MP3 o File) me crean una biblioteca con los
# metadatos que tiene el archivo de audio.
    if ext == '.mp3':
        metadatos_archivo = MP3(archivo, ID3=EasyID3)
    elif ext in ('.ogg', '.flac'):
        metadatos_archivo = File(archivo)

# En base al tipo de archivo, creo una variable con el nombre del campo a
# modificar/añadir. Esta variable tengo que "duplicarla", ya que el estándar
# de vorbis admite el nombre del campo en mayúsculas y en minúsculas. De esta
# forma, genero una variable para el nombre del campo en mayúsculas y otra
# para el nombre del campo en minúsculas.
# Para el caso de id3v2, el nombre del campo es único, por lo que la variable
# duplicada es idéntica a la variable original.
# El nombre del campo lo consulto en los dos diccionarios que creé al principio,
# donde busco el nombre del campo a partir del nombre del metadato que recibe
# la función. De esta forma, con la función hablo un único idioma (claves:
# DISCO, FECHA, GRUPO, TITULO y ORDEN) y los diccionarios lo traducen a id3v2
# o a vorbis.
    campo = ''
    campo_minusc = ''
    if ext == '.mp3':
        campo = id3v2[metadato]
        campo_minusc = campo
    elif ext == '.ogg' or ext == '.flac':
        campo = vorbis[metadato]
        campo_minusc = campo.lower()
    else:
        pass

# Lo siguiente es verificar si el archivo ya tiene el metadato (campo) que se
# quiere añadir/cambiar. La variable metadatos es una biblioteca, y el
# bucle "for" rastrea las "claves" del diccionario, no el valor. Por defecto,
# supongo que el dato no existe.
    existe_dato = False
    minusculas = False
    for campo_archivo in metadatos_archivo:
        if campo_archivo == campo:
            existe_dato = True
            minusculas = False
        elif campo_archivo == campo_minusc:
            existe_dato = True
            minusculas = True
        else:
            pass

# En este punto ya tengo identificado el campo a modificar/añadir (con
# independencia de si se trata de un mp3 o un ogg/flac) y también sé si este
# campo ya existe en el archivo de audio. También sé, porque se entregó
# como dato a la función, si hay que sobre-escribir el dato en caso de que
# exista. Procedo.
#
# Se escriben metadatos en el archivo si no existiesen datos previamente o
# si se da orden de sobre-escribir. El tratamiento es distinto para mp3 que
# para ogg y flac: Por un lado, cada escritura de metadatos sobre mp3 usa sus
# propios métodos. Por otro lado, el estándar sugerido por vorbis permite
# el uso de minúsculas o mayúsculas. Sin embargo, si se usa EasyID3, el
# tratamiento es casi idéntico. La única diferencia es que para id3v2 hay que
# garantizar que no se mezclan versiones (ya que hay la v2, la v2.2, la v2.3,
# etc.) y en el tratamiento hay que indicar la versión que se debe usar.
# Conservo las líneas de código correspondientes al tratamiento "a mano" de
# los campos id3v2:

# Caso .mp3 (usando el estándar a pinrel; esta opción se desestima por
# compleja, ya que obliga a manipular los archivos, tag estándar a tag
# estándar, elaborando código para cada tag particular. Si sólo queremos
# usar un puñado de tags determinados -título, album y poco más- es mejor
# usar EasyID3, que es más simple de programar).
    #if (not existe_dato or sobreescribir) and ext == '.mp3':
        #if campo == 'TALB':
            #metadatos_archivo[campo] = TALB(encodig=3, text=contenido)
        #elif campo == 'TDAT':
            #metadatos_archivo[campo] = TDAT(encodig=3, text=contenido)
        #elif campo == 'TIT1':
            #metadatos_archivo[campo] = TIT1(encodig=3, text=contenido)
        #elif campo == 'TIT2':
            #metadatos_archivo[campo] = TIT2(encodig=3, text=contenido)
        #elif campo == 'TRCK':
            #metadatos_archivo[campo] = TRCK(encodig=3, text=contenido)
        #else:
            #pass
        #metadatos_archivo.save(archivo)

# Caso .mp3 (usando EasyID3 y la conversión a id3v2.3):
    if (not existe_dato or sobreescribir) and ext == '.mp3':
        metadatos_archivo[campo] = contenido
        metadatos_archivo.tags.save(archivo, v2_version=3)

# Caso .ogg y .flac:
    elif (not existe_dato or sobreescribir) and ext in ('.ogg', '.flac'):
        if not minusculas:
            metadatos_archivo[campo] = contenido
        elif minusculas:
            metadatos_archivo[campo_minusc] = contenido
        else:
            pass
        metadatos_archivo.save(archivo)

# No hay más casos, el archivo queda sin tocar, tal cual lo recibió la función.
    else:
        pass

# Fin de la función
    return()


###############################################################################
############ Función para extraer un dato de la ruta del archivo ##############
###############################################################################

# Esta función recibe el nombre de un archivo de audio, y una posición de la
# ruta, y extrae el dato que haya en esa posición. Devuelve ese dato.
# Criterio de posición, ejemplo:
# /home/usuario/música/grupo/año-disco/orden-canción.ogg
#  -6     -5     -4     -3   -2   -1     0     +1
#
def dato_en_posicion(archivo, separadorarchivo, separadordirectorio, posicion):

# Me aseguro que trabajo con un string
#    archivo = str(archivo)

# Separo las distintas partes de la ruta y lo convierto en lista (PurePath crea
# una tupla).
    partes_ruta = PurePath(archivo).parts
    partes_ruta = list(partes_ruta)

# Elimino la extensión del nombre del archivo, para evitar conflictos.
    archivo_puro = partes_ruta.pop()
    (tiene_extension, archivo_puro) = quita_extension(archivo_puro)

# Preparo dos listas para los campos de los metadatos que encierran la ruta
# de directorios y el nombre del archivo.
    lista_campos_directorios = []
    lista_campos_archivo = []

# Separo los campos de la parte con la ruta de directorios
    if separadordirectorio:
        for parte in partes_ruta:
            sub_lista = parte.split(separadordirectorio)
            lista_campos_directorios = lista_campos_directorios + sub_lista
    else:
        lista_campos_directorios = partes_ruta

# Separo los campos de la parte con el nombre del archivo
    if separadorarchivo:
        lista_campos_archivo = archivo_puro.split(separadorarchivo)
    else:
        lista_campos_archivo.append(archivo_puro)

# En este punto ya tengo dos listas de datos: Los datos que encierra la ruta
# de directorios y los datos que encierra el nombre del archivo. Ahora inicio
# una variable para el dato que devuelve la función, y comienzo con el código
# para determinar el dato que está en la posición que se entregó a la función.
    dato = ''

# Por otro lado, inicio una variable int para el mensaje de error. Los errores
# posibles son:
# 0 = Sin error y devuelve el dato
# 1 = No hay dato en la posición indicada
    error = 0

# Determino si el campo buscado está en el nombre del archivo o en la ruta de
# directorios. Eso se sabe por la posición: Si es cero o positivo, el dato
# buscado está en el nombre del archivo. Si es negativo, el dato buscado está
# en alguna parte de la ruta de directorios.
# Primero determino el error y luego el dato (si no hay error).

# El dato está en el nombre del archivo:
    if posicion >= 0:
        # Para determinar el error hay que ver si la posición apunta a un dato
        # de la lista, o si apunta fuera de la lista. Como la posición se
        # cuenta desde cero, hay que sumarle uno para comparar con la cantidad
        # total de campos disponibles.
        if (posicion + 1) > len(lista_campos_archivo):
            error = 1
        else:
            dato = lista_campos_archivo[posicion]
            error = 0

# El dato está en la lista de directorios:
    elif posicion < 0:
        # Para determinar el error hay que ver si la posición apunta a un dato
        # de la lista, o si apunta fuera de la lista. En el caso de la lista de
        # directorios, la posición se indica en números negativos, a contar
        # de alante a atrás, es decir, de la última a la primera posición de
        # lista. Lo primero es traducir esa posición en coordenadas "negativas"
        # a coordenadas "positivas". Esto se hace facilmente sin más que
        # sumarle a la posición "negativa" el número de campos que contiene
        # la lista.
        posicion_positiva = len(lista_campos_directorios) + posicion
        if (posicion_positiva + 1) > len(lista_campos_directorios):
            error = 1
        else:
            dato = lista_campos_directorios[posicion_positiva]
            error = 0

# No hay más opciones:
    else:
        error = 1

# Fin de la función. Devuelvo el error y el dato si no hubiese error.
    return(error, dato)