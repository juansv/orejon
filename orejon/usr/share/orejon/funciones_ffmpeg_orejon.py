#! /usr/bin/python3
# -*- coding: utf-8 -*-

# Funciones de manejo de ffmpeg para uso del programa Orejón.

from subprocess import Popen, PIPE, call
from funciones_nombres_orejon import quita_extension, extension


###############################################################################
######### Función para determinar el bit rate del archivo de audio ############
###############################################################################

# Esta función invoca al comando ffmpeg para determinar el bit rate con el que
# fue ripeado el archivo de audio. En realidad, se utiliza el comando ffprobe,
# que forma parte del paquete ffmpeg. La diferencia entre ambos es que ffmpeg
# está concebido para editar archivos multimedia, por lo que espera por un
# archivo de entrada (el que se va a editar) y uno de salida (el archivo
# editado). Pero si sólo se quiere conocer alguna información del archivo
# multimedia, tenemos ffprobe, que no espera por ningún archivo de salida. Las
# opciones y demás de ffprobe y ffmpeg son básicamente las mismas, pero si a
# ffmpeg no se le indica un archivo de salida, devuelve un mensaje de error.
# La función espera por la ruta del archivo que queremos examinar y devuelve
# el bit rate como un entero.


def bit_rate(ruta_archivo):

# Secuencia de argumentos del comando ffmpeg

# a0 es el comando. En este caso se usa ffprobe, porque ffmpeg espera siempre
# por un archivo de salida, y sólo queremos consultar el bit rate.
    a0 = 'ffprobe'

# a1 y a2 es la opción para indicarle a ffprobe sobre qué archivo vamos a
# realizar la consulta. a2 es la ruta del archivo.
    a1 = '-i'
    a2 = ruta_archivo

# a3 y a4 es una opción para indicarle a ffprobe que no nos muestre información
# alguna (quiet). De esta forma no mostrará más datos que los que le pidamos.
    a3 = '-v'
    a4 = 'quiet'

# a5 y a6 le indica a ffprobe que trabaje sobre el corte de audio "0". Recordad
# que un archivo multimedia puede tener varios cortes de audio y video.
    a5 = '-select_streams'
    a6 = 'a:0'

# a7 y a8 le indica a ffprobe que, de todos los datos disponibles sobre el
# archivo de audio, sólo me interesa el bit_rate.
    a7 = '-show_entries'
    a8 = 'stream=bit_rate'

# a9 y a10 le indica a ffprobe que no muestre las etiquetas de los datos que
# estoy pidiendo.
    a9 = '-of'
    a10 = 'default=noprint_wrappers=1'

# a11 le indica a ffprobe que muestre el dato con sus unidades de medida.
# a12 le indica a ffprobe que esas unidades de medida se muestren con prefijo
# "kilo", "mega", etc.
    a11 = '-unit'
    a12 = '-prefix'

# En esta línea se construye el comando que vamos a ejecutar
    consulta_ffmpeg = [a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12]

# En esta línea se ejecuta el comando anterior y se recoge lo que devuelve por
# la salida estándar (stdout) en el archivo resultado_ffmpeg. Añado las
# opciones bufsize=1 y universal_newlines=True para que Popen genere un archivo
# de texto, en vez de un binario.
    resultado_ffmpeg = Popen(consulta_ffmpeg, stdout=PIPE, bufsize=1,
        universal_newlines=True)

# Cargo el contenido del archivo (una línea) en una variable que convierto en
# un string para poder manipularla, y cierro el archivo.
    bitrate = resultado_ffmpeg.stdout.read()
    bitrate = str(bitrate)
    resultado_ffmpeg.stdout.close()

# Determino si hay un valor numérico entre el signo "="
# y las unidades "Kbit/s", valor que correspondería con el bit rate.
# De existir, convierte el número en int y lo devuelve. De no existir,
# puede que estemos antes un archivo de audio sin pérdidas. Lo comprueba y
# si no, devuelve 0. Sólo verifica los siguientes archivos sin pérdida:
# FLAC, WAV y AIFF. No contemplo el archivo sin pérdidas de apple, el ALAC.
# En caso de un archivo sin pérdidas devuelve un valor de bit rate de
# cien mil Kbit/s. Cien mil es un número arbitrario, simplemente es lo
# bastante alto para no generar confusiones.
    inicio = bitrate.find("=") + 1
    fin = bitrate.find("Kbit/s") - 1
    if bitrate[inicio:fin].isdigit():
        bitrate = int(bitrate[inicio:fin])
    else:
        (tiene_extension, extension_archivo) = extension(ruta_archivo)
        extensiones_lossless = ['.wav', '.flac', '.aiff']
        for lossless in extensiones_lossless:
            if extension_archivo == lossless:
                bitrate = 100000
                break
            else:
                bitrate = 0

# Fin de la función.
    return(bitrate)


###############################################################################
#################### Función para comprimir en ogg o mp3 ######################
###############################################################################

# Esta función convierte el archivo de audio, cuya ruta hay que entregarle,
# en un archivo comprimido con el bit rate que se le indique. El formato de
# compresión hay que indicarlo con la extensión buscada (ogg, mp3...). Por
# último, borra el archivo original si se le indica true. El bit rate hay que
# indicarlo en Kbit/s.

def comprime(ruta_archivo, bit_rate, extension_salida, borrar):

# Secuencia de argumentos del comando ffmpeg

# a0 es el comando ffmpeg
    a0 = 'ffmpeg'

# a1 y a2 indican el archivo de entrada
    a1 = '-i'
    a2 = ruta_archivo

# a3 y a4 indican a ffmpeg que no muestre información por la salida estándar.
    a3 = '-v'
    a4 = 'quiet'

# a5 y a6 indican el bit rate. Como ffmpeg lo quiere en bit/s, lo convierto a
# int y lo multiplico por mil. Luego, lo vuelvo a str otra vez.
    bit_rate = int(bit_rate)
    bit_rate = bit_rate * 1000
    bit_rate = str(bit_rate)
    a5 = '-b:a'
    a6 = bit_rate

# a7 y a8 especifican el codec de audio a utilizar (sólo dos opciones, vorbis
# o lame)
    if extension_salida == 'ogg':
        codec = 'libvorbis'
    elif extension_salida == 'mp3':
        codec = 'libmp3lame'
    else:
        codec = 'libvorbis'
    a7 = '-acodec'
    a8 = codec

# a9 especifica que no se copie ningún corte de video al archivo de audio
# resultante. Es necesario porque se observa que algunos archivos de audio
# tienen una suerte de "video oculto" y ffmpeg tratará de encapsular también
# este corte de video en el contenedor que se indique. Es especialmente
# problemático con los ogg, ya que permiten contener vídeos y ffmpeg hará la
# conversión de ese corte de vídeo fantasma al códec de vorbis, lo que puede
# generar problemas con algunos reproductores de música. En el caso de los
# mp3 no se observa tanto problema, ya que la extensión mp3 no admite contener
# vídeo, por lo que el corte de vídeo se elimina por si sólo al codificar en
# mp3. No obstante, la opción "-vn" (podría traducirse por "video no") no
# da problemas tanto se especifique ogg como se especifique mp3 como archivo
# de salida.
    a9 = '-vn'

# a10 es el archivo de salida. Como el archivo de salida debe llevar la
# extensión del archivo comprimido, le quito la extensión al archivo original
# y se la cambio por el tipo de archivo que se entregó a la función. Con toda
# esta información compongo un string que es el que cargo en a10. Pero si la
# extensión del archivo original es la misma que la del archivo de salida,
# debo modificar ligeramente el nombre del archivo de salida para que no
# coincidan ambos nombres; para ello añado el texto "_orj", de "orejón".
    (tiene_extension, salida) = quita_extension(ruta_archivo)
    (tiene_extension, extension_entrada) = extension(ruta_archivo)
    if extension_entrada == '.' + extension_salida:
        salida = salida + '_orj' + '.' + extension_salida
    else:
        salida = salida + '.' + extension_salida
    a10 = salida

# En esta línea se construye el comando que vamos a ejecutar
    orden_ffmpeg = [a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10]

# En esta línea se ejecuta el comando anterior. No hace falta capturar la
# salida, por lo que se utiliza el método call.
    call(orden_ffmpeg)

# Si se entrega valor true en borrar, se borra el archivo de entrada.
    if borrar:
        call(['rm', ruta_archivo])
    else:
        pass

# Fin de la función
    return()