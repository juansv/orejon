#! /usr/bin/python3
# -*- coding: utf-8 -*-

# Orejon es un programa que comprime TODOS los archivos de audio que tengas
# en un directorio y los subdirectorios que cuelguen de él.
# El objeto de Orejon es optimizar el espacio de unidades de memoria que
# habitualmente se utilizan para llevar música contigo (llaveros usb,
# reproductores de mp3, etc.).
# Orejon-nombres limpia algunos caracteres de los nombres de los archivos de
# audio, ya que algunos reproductores de mp3 no leen los archivos si los
# nombres tienen letras con tildes (á, é, í, ó, ú) u otros símbolos (#, @...).
# Orejon-nombres consulta una tabla con los símbolos conflictivos y los cambia
# por otros (si figura un símbolo alternativo en la tabla).

import argparse
from pathlib import PurePath
from subprocess import Popen, PIPE, call
from funciones_nombres_orejon \
    import extension, quita_extension, limpia_nombre, quita_orj_final


# AQUÍ LOS VALORES QUE SE ENTREGAN AL PROGRAMA COMO ARGUMENTOS:

# Iniciar una lista de argumentos para los argumentos
lista_argumentos = argparse.ArgumentParser(description=
    "Arregla el nombre de los archivos de audio del directorio que se indique")

# Aquí el usuario indica que se borre la coletilla "_orj" de los archivos que
# manipuló Orejón.
lista_argumentos.add_argument("-orj", "--orejon", action="store_true",
    default=False, help="solamente borra '_orj' de los archivos de audio")

# Esta es la ruta del directorio raíz sobre el que se hará la búsqueda de los
# archivos de audio que se van a renombrar, si cumplen los requisitos.
lista_argumentos.add_argument("directorio",
    help="directorio donde se buscarán los archivos de audio")

# Si la ruta se indica en forma relativa, el script de lanzamiento de Orejon
# captura la raíz de la ruta y la entrega a Orejon.
lista_argumentos.add_argument("raiz")


# CARGA DE LAS VARIABLES DEL PROGRAMA Y MOSTRAR AL USUARIO PARA CONFIRMACIÓN

# Inicializamos el uso de argumentos
argumento = lista_argumentos.parse_args()

# Determinación del directorio de trabajo. Con los métodos de pathlib determino
# si se entregó el directorio en forma absoluta o no. Si se entregó en forma
# absoluta, la ruta es igual al argumento directorio. Si no se entregó en
# forma absoluta, se compone la ruta con el argumento del directorio relativo
# y la raiz que captura el script de lanzamiento.
raiz = argumento.raiz
directorio = argumento.directorio
if PurePath(directorio).is_absolute():
    ruta = str(directorio)
else:
    ruta = str(PurePath(raiz).joinpath(directorio))

# Quitar '_orj' del nombre de los archivos
orj = argumento.orejon

# Pide confirmación para hacer los cambios
print("")  # línea en blanco por motivos de legibilidad.
print("Orejón-nombres buscará todos los archivos de audio que cuelguen del",
    "directorio (y sus subdirectorios):")
print("")  # línea en blanco por motivos de legibilidad.
print(ruta)
print("")  # línea en blanco por motivos de legibilidad.
if orj:
    print("Y les eliminará la coletilla '_orj' de aquellos archivos que",
        ", previamente, fueron comprimidos por Orejón.")
    print("")  # línea en blanco por motivos de legibilidad.
else:
    print("Estos archivos se renombrarán según los criterios de la tabla",
        "/etc/orejon/orejon-nombres.list")
    print("")  # línea en blanco por motivos de legibilidad.
confirmacion = input("¿Continuar? [s/N] ")
if confirmacion == 'S' or confirmacion == 's':
    salir = False
else:
    salir = True


# PASO 0: CONSEGUIR LA LISTA DE ARCHIVOS DEL DIRECTORIO A TRATAR.
# Lanzo la consulta al SO sobre el contenido del directorio. La consulta la
# realizo con el comando "find 'ruta' -type f". "find" busca el archivo que se
# le indique. Al no indicar ningún archivo concreto, si no un directorio,
# "find" busca todos los archivos del directorio. Le añado la opción "-type f"
# para que sólo me relacione los nombres de los archivos encontrados, y
# excluya el nombre de los subdiretorios.
# La ejecución de este comando del SO se lo encargo al módulo subprocess.
# Utilizo el subobjeto Popen y el método PIPE para cargar la salida (stdout)
# del "find" en un fichero. Ojo, que Popen crea por defecto un archivo binario,
# no de texto. Para que capture la información como texto hay que indicarle
# las opciones "bufsize=1, universal_newlines=True". Una vez capturado el
# texto, tengo que limpiar el retorno de carro "\n" que hay al final de cada
# línea.
lista_archivos = Popen(['find', ruta, '-type', 'f'], stdout=PIPE, bufsize=1,
    universal_newlines=True)

# Leo secuencialmente el fichero con la lista de archivos, línea a línea, es
# decir, archivo a archivo.
for archivo in lista_archivos.stdout:

# Si el usuario se raja, salimos
    if salir:
        break
    else:
        pass

# Ahora, proceso la info que hay en el fichero de texto que crea Popen:
# Convierto en string cada una de las entradas del fichero y les quito el fin
# de línea "\n", que lo quito en el mismo acto de convertir en string, con
# la opción [:-1] que quita el último caracter.
    archivo = str(archivo[:-1])

# Creo una variable para el nombre del archivo sin la extensión y otra para la
# extensión
    (tiene_extension, extension_archivo) = extension(archivo)
    (tiene_extension, archivo_sin_extension) = quita_extension(archivo)

# Si el archivo no tiene extensión, termino.
    if not tiene_extension:
        break
    else:
        pass

# En este punto ya tengo un string con el nombre y la ruta completa del
# archivo, otro string con el nombre y la ruta completa, pero el nombre sin
# la extensión de archivo y, por último, otro string con la extensión (incluido
# el punto).
# Empiezo con el algoritmo principal del programa:

# PASO 1: DETERMINAR SI SE TRATA DE UN ARCHIVO DE AUDIO
# Leo la extensión del archivo y la comparo con una lista de extensiones de
# ficheros de audio. Por defecto, supongo que no es de audio, para no liarla.
    archivo_audio = False

    # La lista de extensiones de archivos de audio la saco de un archivo de
    # texto que guardo en /etc/orejon/ojeron.list, por lo tanto, lo primero
    # que hago es abrir ese archivo y crear la lista de extensiones
    extensiones = []
    listado_extensiones_orejon = open('/etc/orejon/orejon.list', 'r')
    for linea in listado_extensiones_orejon:
        # El archivo /etc/orejon/ojeron.list admite comentarios, los ignoro
        if linea[0] == "#" or linea == "\n":
            pass
        else:
        # Al leer un elemento de la lista, se carga el salto de línea "\n", con
        # la opción [:-1] elimino el salto de línea.
            extensiones.append(linea[:-1])
    # Cierro el archivo /etc/orejon/ojeron.list
    listado_extensiones_orejon.close()

    # Procedo con la comprobación de si coincide la extensión del archivo con
    # alguno de la lista de extensiones de audio que reconoce Orejón.
    for extension_audio in extensiones:
        if extension_archivo == extension_audio:
            archivo_audio = True
            break
        else:
            pass

# PASO 2: PROCEDER CON EL CAMBIO DE NOMBRE DE LOS ARCHIVOS DE AUDIO
# En este punto ya tengo identificados los archivos de audio del directorio y
# puedo hacer los cambios de nombre. Defino un string donde cargar el nombre
# modificado. Obviamente, antes de modificar, el archivo modificado es igual
# que el archivo original.
    archivo_modificado = archivo

# Quitamos '_orj' si procede (es decir, si es un archivo de audio y pedí quitar
# el "_orj" en las opciones de orejon-nombres)
# CONVERTIR ESTO EN UNA FUNCIÓN
    if archivo_audio and orj:
        archivo_modificado = quita_orj_final(archivo_modificado)
    else:
        pass

# Limpiamos los caracteres de /etc/orejon/orejon-nombre.list si procede (es
# decir, si es un archivo de audio). Para esta limpieza tengo una función a
# la que tengo que entregar el nombre del archivo completo (con la ruta y la
# extensión de archivo).
    if archivo_audio and not orj:
        archivo_modificado = limpia_nombre(archivo_modificado)
    else:
        pass

# Una vez editado el nombre, procedemos a modificar el nombre del archivo
    if archivo != archivo_modificado:
        print("")  # línea en blanco por motivos de legibilidad.
        print("Se procede a cambiar el nombre del archivo:")
        print(archivo)
        print("por:")
        print(archivo_modificado)
        call(['mv', archivo, archivo_modificado])
    else:
        pass

# Cerramos el archivo con la lista de archivos del directorio.
lista_archivos.stdout.close()

print("")  # línea en blanco por motivos de legibilidad.
print("Trabajo terminado.")