#! /usr/bin/python3
# -*- coding: utf-8 -*-

# Orejon es un programa que comprime TODOS los archivos de audio que tengas
# en un directorio y los subdirectorios que cuelguen de él.
# El objeto de Orejon es optimizar el espacio de unidades de memoria que
# habitualmente se utilizan para llevar música contigo (llaveros usb,
# reproductores de mp3, etc.).
# Orejon-meta ayuda al usuario a introducir metadatos en los archivos de
# audio. Si el usuario mantiene sus archivos de audio ordenados en carpetas
# que contengan el nombre del intérprete, el nombre del disco, etc. y, además,
# el propio nombre del archivo también tiene datos relevantes (nombre de la
# canción, orden de la canción en el disco, etc.), orejon-meta permite extraer
# toda esa información y volcarla como metadatos en los archivos de audio de
# forma automática.

import argparse
from pathlib import PurePath
from subprocess import Popen, PIPE
from funciones_nombres_orejon import extension
from funciones_metadatos_orejon import escribe_metadato, dato_en_posicion


##############################################################################
# AQUÍ LOS VALORES QUE SE ENTREGAN AL PROGRAMA COMO ARGUMENTOS:

# Iniciar una lista de argumentos para los argumentos.
lista_argumentos = argparse.ArgumentParser(description=
    "Añade metadatos en los archivos de audio del directorio que se indique")

# Incompatibilidad de título
elige_titulo = lista_argumentos.add_mutually_exclusive_group()

# Incompatibilidad de orden
elige_orden = lista_argumentos.add_mutually_exclusive_group()

# Incompatibilidad de grupo
elige_grupo = lista_argumentos.add_mutually_exclusive_group()

# Incompatibilidad de disco
elige_disco = lista_argumentos.add_mutually_exclusive_group()

# Incompatibilidad de año
elige_fecha = lista_argumentos.add_mutually_exclusive_group()

# Aquí se indica si se quieren sobreescribir los metadatos que ya tiene el
# archivo de audio.
lista_argumentos.add_argument("-s", "--sobreescribir", action="store_true",
    default=False, help="sobreescribe los metadatos existentes")

# Toma el título de la canción de la posición de la ruta del archivo que se
# indique con un número entero.
elige_titulo.add_argument("-t", "--titulo", type=int,
    #default=0,
    help="toma el título de la canción de la posición que se indique")

# Toma el título de la canción que se indíque explícitamente.
elige_titulo.add_argument("-T", "--TITULO", type=str,
    #default="",
    help="toma el título de la canción que se indique explícitamente")

# Toma el orden de la canción de la posición de la ruta del archivo que se
# indique con un número entero.
elige_orden.add_argument("-o", "--orden", type=int,
    #default=0
    help="toma el orden de la canción de la posición que se indique")

# Toma el título del disco que se indíque explícitamente.
elige_orden.add_argument("-O", "--ORDEN", type=str,
    #default="",
    help="toma el orden de la canción que se indique explícitamente")

# Toma el nombre del grupo de la posición de la ruta del archivo que se
# indique con un número entero.
elige_grupo.add_argument("-g", "--grupo", type=int,
    #default=0,
    help="toma el nombre del grupo de la posición que se indique")

# Toma el nombre del grupo que se indíque explícitamente.
elige_grupo.add_argument("-G", "--GRUPO", type=str,
    #default="",
    help="toma el nombre del grupo que se indique explícitamente")

# Toma el título del disco de la posición de la ruta del archivo que se
# indique con un número entero.
elige_disco.add_argument("-d", "--disco", type=int,
    #default=0,
    help="toma el título del disco de la posición que se indique")

# Toma el título del disco que se indíque explícitamente.
elige_disco.add_argument("-D", "--DISCO", type=str,
    #default="",
    help="toma el título del disco que se indique explícitamente")

# Toma el año del disco de la posición de la ruta del archivo que se
# indique con un número entero.
elige_fecha.add_argument("-f", "--fecha", type=int,
    #default=0,
    help="toma el año del disco de la posición que se indique")

# Toma el año del disco que se indíque explícitamente.
elige_fecha.add_argument("-F", "--FECHA", type=str,
    #default="",
    help="toma el año del disco que se indique explícitamente")

# Separador de campos dentro del nombre del archivo.
lista_argumentos.add_argument("-sa", "--separadorarchivo", type=str,
    default="",
    help="carácter separador de campos en el nombre del archivo")

# Separador de campos dentro de los nombres de los directorios.
lista_argumentos.add_argument("-sd", "--separadordirectorio", type=str,
    default="",
    help="carácter separador de campos en el nombre de los directorios")

# Esta es la ruta del directorio raíz sobre el que se hará la búsqueda de los
# archivos de audio sobre los que se va a escribir los metadatos.
lista_argumentos.add_argument("directorio",
    help="directorio donde están los archivos de audio")

# Si la ruta se indica en forma relativa, el script de lanzamiento de Orejon
# captura la raíz de la ruta y la entrega a Orejon.
lista_argumentos.add_argument("raiz")


##############################################################################
# CARGA DE LAS VARIABLES QUE SE ENTREGAN COMO ARGUMENTOS

# Inicializamos el uso de argumentos
argumento = lista_argumentos.parse_args()

# Determinación del directorio de trabajo. Con los métodos de pathlib determino
# si se entregó el directorio en forma absoluta o no. Si se entregó en forma
# absoluta, la ruta es igual al argumento directorio. Si no se entregó en
# forma absoluta, se compone la ruta con el argumento del directorio relativo
# y la raiz que captura el script de lanzamiento.
raiz = argumento.raiz
directorio = argumento.directorio
if PurePath(directorio).is_absolute():
    ruta = str(directorio)
else:
    ruta = str(PurePath(raiz).joinpath(directorio))

# Determinar si se sobre-escribe el metadato en caso de que pre-exista.
sobreescribir = argumento.sobreescribir

# Posición relativa del título de la canción
titulo_relativo = argumento.titulo

# Nombre explícito del título de la canción
titulo_explicito = argumento.TITULO

# Posición relativa del orden de la canción
orden_relativo = argumento.orden

# Orden de la canción explícito
orden_explicito = argumento.ORDEN

# Posición relativa del nombre del grupo
grupo_relativo = argumento.grupo

# Nombre explícito del grupo
grupo_explicito = argumento.GRUPO

# Posición relativa del nombre del disco
disco_relativo = argumento.disco

# Nombre del disco explícito
disco_explicito = argumento.DISCO

# Posición relativa del año del disco
fecha_relativo = argumento.fecha

# Año del disco explícito
fecha_explicito = argumento.FECHA

# Determinar el carácter que separa campos en el nombre del archivo
separador_archivo = argumento.separadorarchivo

# Determinar el carácter que separa campos en el nombre del directorio
separador_directorio = argumento.separadordirectorio


##############################################################################
# INFORMAR Y PEDIR CONFIRMACIÓN

# Informar por pantalla
print("")  # línea en blanco por motivos de legibilidad.
print("Orejón buscará todos los archivos de audio que cuelguen del directorio",
    "(y sus subdirectorios):")
print("")  # línea en blanco por motivos de legibilidad.
print(ruta)
print("")  # línea en blanco por motivos de legibilidad.
print("y modificará los metadatos de estos archivos según las instrucciones",
    "indicadas")
print("")  # línea en blanco por motivos de legibilidad.
if sobreescribir:
    print("Si el metadato ya existiese, se sobre escribirá con el nuevo dato.")
else:
    print("Si el metadato ya existiese, no se tocará, permanece igual.")
print("")  # línea en blanco por motivos de legibilidad.

# Se pide confirmación
confirmacion = input("¿Continuar? [s/N] ")
if confirmacion == 'S' or confirmacion == 's':
    salir = False
else:
    salir = True


##############################################################################
# ALGORITMO DEL PROGRAMA

# PASO 0: CONSEGUIR LA LISTA DE ARCHIVOS DEL DIRECTORIO A TRATAR.
# Lanzo la consulta al SO sobre el contenido del directorio. La consulta la
# realizo con el comando "find 'ruta' -type f". "find" busca el archivo que se
# le indique. Al no indicar ningún archivo concreto, si no un directorio,
# "find" busca todos los archivos del directorio. Le añado la opción "-type f"
# para que sólo me relacione los nombres de los archivos encontrados, y
# excluya el nombre de los subdiretorios.
# La ejecución de este comando del SO se lo encargo al módulo subprocess.
# Utilizo el subobjeto Popen y el método PIPE para cargar la salida (stdout)
# del "find" en un fichero. Ojo, que Popen crea por defecto un archivo binario,
# no de texto. Para que capture la información como texto hay que indicarle
# las opciones "bufsize=1, universal_newlines=True". Una vez capturado el
# texto, tengo que limpiar el retorno de carro "\n" que hay al final de cada
# línea.
lista_archivos = Popen(['find', ruta, '-type', 'f'], stdout=PIPE, bufsize=1,
    universal_newlines=True)

# Leo secuencialmente el fichero con la lista de archivos, línea a línea, es
# decir, archivo a archivo.
for archivo in lista_archivos.stdout:

# Si el usuario se raja, salimos
    if salir:
        break
    else:
        pass

# Ahora, proceso la info que hay en el fichero de texto que crea Popen:
# Convierto en string cada una de las entradas del fichero y les quito el fin
# de línea "\n", que lo quito en el mismo acto de convertir en string, con
# la opción [:-1] que quita el último caracter.
    archivo = str(archivo[:-1])

# PASO 1: DETERMINAR SI SE TRATA DE UN ARCHIVO DE AUDIO VÁLIDO (MP3, OGG, FLAC)
# Leo la extensión del archivo y la comparo con una lista de extensiones de
# ficheros de audio. Por defecto, supongo que no es de audio, para no liarla.
# También verifico que el archivo tenga extensión, por si acaso no la tuviese.
    extensiones = ('.mp3', '.ogg', '.flac')
    archivo_audio = False
    (tiene_extension, extension_archivo) = extension(archivo)

# En caso que el archivo tenga extensión, compruebo que está en la lista de
# extensiones de audio que admite Orejón, en cuyo caso pongo en True la
# variable que me identifica que el archivo es de audio.
    if tiene_extension:
        # Procedo con la comprobación de si coincide la extensión del archivo
        # con alguno de la lista de extensiones de audio que reconoce Orejón.
        for extension_audio in extensiones:
            if extension_archivo == extension_audio:
                archivo_audio = True
                break
            else:
                pass
    # Si no tiene extensión no hago nada, ya que ya tengo puesto a False la
    # variable que identifica como fichero de audio al archivo.
    else:
        pass

# PASO 2: MODIFICAR EL METADATO QUE PROCEDA
# Rastreo, metadato a metadato, los 5 que Orejon-meta sabe procesar y modifico
# el metadato en el archivo si procede.
# Primero se comprueba si se aportó, como argumento, una posición relativa del
# metadato. Si es así, se busca ese dato en la ruta y nombre del archivo. Si
# no se encuentra ningún dato válido en esa posición, se devuelve un mensaje
# de error. Si se localiza un dato válido en esa posición, se escribe o sobre
# escribe, según proceda.
# Luego se comprueba si se aportó, como argumento, una dato explícito. Si es
# así, se procede directamente a escribir o sobreescribir, según proceda.
# Nota: en la comprobación del dato relativo, hay que valorar también si, vía
# argumentos, se le dio el valor cero "0", ya que Python interpreta, por
# defecto, que cero es que no se aportó el argumento. Por lo tanto, la pregunta
# no puede ser sólo "if dato_relativo", tiene que ser
# "if (dato_relativo or dato_relativo == 0)"

# Para el TÍTULO DE LA CANCIÓN
    if archivo_audio and (titulo_relativo or titulo_relativo == 0):
        (error, dato) = dato_en_posicion(archivo, separador_archivo,
            separador_directorio, titulo_relativo)
        if error == 0:
            escribe_metadato('TITULO', dato, sobreescribir, archivo)
        elif error == 1:
            print("")  # línea en blanco por motivos de legibilidad.
            print("No se localiza un título de canción válido en la posición",
                titulo_relativo, "del archivo:")
            print(archivo)
        else:
            pass
    elif archivo_audio and titulo_explicito:
        escribe_metadato('TITULO', titulo_explicito, sobreescribir, archivo)
    else:
        pass

# Para el ORDEN DE LA CANCIÓN
    if archivo_audio and (orden_relativo or orden_relativo == 0):
        (error, dato) = dato_en_posicion(archivo, separador_archivo,
            separador_directorio, orden_relativo)
        if error == 0:
            escribe_metadato('ORDEN', dato, sobreescribir, archivo)
        elif error == 1:
            print("")  # línea en blanco por motivos de legibilidad.
            print("No se localiza un orden de canción válido en la posición ",
                orden_relativo, "del archivo:")
            print(archivo)
        else:
            pass
    elif archivo_audio and orden_explicito:
        escribe_metadato('ORDEN', orden_explicito, sobreescribir, archivo)
    else:
        pass

# Para el NOMBRE DEL GRUPO
    if archivo_audio and (grupo_relativo or grupo_relativo == 0):
        (error, dato) = dato_en_posicion(archivo, separador_archivo,
            separador_directorio, grupo_relativo)
        if error == 0:
            escribe_metadato('GRUPO', dato, sobreescribir, archivo)
        elif error == 1:
            print("")  # línea en blanco por motivos de legibilidad.
            print("No se localiza un nombre de grupo válido en la posición ",
                grupo_relativo, "del archivo:")
            print(archivo)
        else:
            pass
    elif archivo_audio and grupo_explicito:
        escribe_metadato('GRUPO', grupo_explicito, sobreescribir, archivo)
    else:
        pass

# Para el NOMBRE DEL DISCO
    if archivo_audio and (disco_relativo or disco_relativo == 0):
        (error, dato) = dato_en_posicion(archivo, separador_archivo,
            separador_directorio, disco_relativo)
        if error == 0:
            escribe_metadato('DISCO', dato, sobreescribir, archivo)
        elif error == 1:
            print("")  # línea en blanco por motivos de legibilidad.
            print("No se localiza un nombre de disco válido en la posición ",
                disco_relativo, "del archivo:")
            print(archivo)
        else:
            pass
    elif archivo_audio and disco_explicito:
        escribe_metadato('DISCO', disco_explicito, sobreescribir, archivo)
    else:
        pass

# Para el AÑO DEL DISCO (campo FECHA)
    if archivo_audio and (fecha_relativo or fecha_relativo == 0):
        (error, dato) = dato_en_posicion(archivo, separador_archivo,
            separador_directorio, fecha_relativo)
        if error == 0:
            escribe_metadato('FECHA', dato, sobreescribir, archivo)
        elif error == 1:
            print("")  # línea en blanco por motivos de legibilidad.
            print("No se localiza un año de disco válido en la posición ",
                fecha_relativo, "del archivo:")
            print(archivo)
        else:
            pass
    elif archivo_audio and fecha_explicito:
        escribe_metadato('FECHA', fecha_explicito, sobreescribir, archivo)
    else:
        pass

# Cierro archivos
lista_archivos.stdout.close()

print("")  # línea en blanco por motivos de legibilidad.
print("Trabajo terminado.")