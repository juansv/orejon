#! /usr/bin/python3
# -*- coding: utf-8 -*-

# Módulos: Utilizo el módulo Pathlib, que es de la librería estándar de
# Python 3, y que permite manipular nombres de archivos, sus extensiones,
# sus rutas, etc.
from pathlib import PurePath

# Funciones que manejan los nombres de los archivos


###############################################################################
######## Función para determinar la extensión de un archivo de audio ##########
###############################################################################

# Utilizo la librería estándar Pathlib, que tiene un objeto, PurePath, cuyos
# métodos sonsacan las extensiones, rutas, etc, de los archivos que se le
# indiquen
def extension(archivo):
    extension = PurePath(archivo).suffix
    if extension != "":
        tiene_extension = True
    else:
        tiene_extension = False
    return(tiene_extension, extension)

###########################################################
# Se me había ocurrido hacerlo como se vé en las próximas #
# líneas de código, pero es mucho más fácil usar la li-   #
# brería estándar Pathlib.                                #
###########################################################

## Esta función recibe el nombre del archivo, inclusive el nombre con la ruta
## completa del archivo, y rastrea de atrás hacia adelante hasta encontrar un
## punto ".", lo que significa que leyó la extensión. Devuelve la extensión
## con el punto (.mp3, .ogg, .flac, etc.).
## NOTA: esta función espera el nombre del archivo como un string. Por si acaso
## no lo fuese, lo primero que hace es convertirlo a string.
#def extension(nombre_archivo):

## Defino un puntero para recorrer el strig con el nombre del archivo de atrás
## hacia adelante. Para ello determino la longitud del string que contiene el
## nombre del archivo y le quito uno para que se posicione en el último
## caracter del string. También inicializo otro string donde voy a cargar la
## extensión.
    #nombre_archivo = str(nombre_archivo)
    #i = len(nombre_archivo)
    #i = i - 1
    #ext = ""

## Recorro el string del nombre del archivo hasta encontrar un punto. Hasta
## que encuentre el punto, voy cargando los caracteres que localizo en el
## string donde almaceno la extensión.
    #while i > 0:
        #caracter_ext = nombre_archivo[i]
        #ext = caracter_ext + ext
        #if nombre_archivo[i] == ".":
            #i = 0
        #else:
            #i = i - 1

## Fin de la función. Devuelvo la extensión con el punto (.mp3, etc.)
    #return(ext)


###############################################################################
########## Función para eliminar la extensión de un archivo de audio ##########
###############################################################################

# Utilizo la librería estándar Pathlib, que tiene un objeto, PurePath, cuyos
# métodos sonsacan las extensiones, rutas, etc, de los archivos que se le
# indiquen. Primero determino si el archivo tiene extensión y, si la tiene,
# creo un string con el texto del archivo original menos los caracteres de la
# extensión. Si el archivo no tiene extensión, la función devuelve el archivo
# original.
def quita_extension(archivo_original):

# Determino si el archivo tiene extensión
    extension = PurePath(archivo_original).suffix
    if extension != "":
        tiene_extension = True
    else:
        tiene_extension = False

# Inicializo dos strings, uno para el nombre del archivo original y otro para
# el nombre del archivo sin la extensión
    archivo_original = str(archivo_original)
    archivo_sin_ext = ""

# Resuelvo el nombre del archivo sin la extensión
    if tiene_extension:
        archivo_sin_ext = archivo_original[:-len(extension)]
    else:
        archivo_sin_ext = archivo_original

# Terminamos la función devolviendo True o False para indicar si el archivo
# tiene extensión. Si tiene extensión devuelve el nombre sin la extensión y
# si no la tiene devuelve el nombre tal cual.
    return(tiene_extension, archivo_sin_ext)

###########################################################
# Se me había ocurrido hacerlo como se vé en las próximas #
# líneas de código, pero es mucho más fácil usar la li-   #
# brería estándar Pathlib.                                #
###########################################################

## Esta función recibe el nombre de un archivo (con o sin su ruta completa) y
## le quita la extensión al archivo. Lo primero que hace es localizar en qué
## posición del string con el nombre del archivo está la extensión. Para ello,
## rastrea todos los caracteres del string de atrás hacia adelante, hasta
## encontrar un punto. Luego rastrea todos los caracteres del string de alante
## a atrás, hasta la posición donde empieza la extensión, momento en que se
## para. Devuelve un string con el mismo nombre pero sin la extensión.
## Hay que prever que el archivo pudiera no tener extensión. Para ello, durante
## el rastreo del punto "." de la extensión, vigilo que no me aparezca una
## barra "/" de directorio. Si aparece una barra antes que un punto, significa
## que el archivo no tiene extensión. Finalmente, devuelvo el nombre del archivo
## sin la extensión y una variable booleana que me dice si el archivo tiene
## extensión o no (True = tiene extensión; False = no tiene extensión).
## NOTA: esta función espera el nombre del archivo como un string. Por si acaso
## no lo fuese, lo primero que hace es convertirlo a string.

## Defino un puntero para recorrer el strig con el nombre del archivo de atrás
## hacia adelante. Para ello determino la longitud del string que contiene el
## nombre del archivo y le quito uno para que se posicione en el último
## caracter del string (los caracteres del string se cuentan desde cero).
## También inicializo un string donde voy a almacenar el nombre del archivo
## sin la extensión.
    #archivo_original = str(archivo_original)
    #i = len(archivo_original)
    #i = i - 1
    #archivo_sin_ext = ""

## Lanzo un bucle que rastrea, de atrás hacia adelante, los caracteres del
## string que tiene el nombre del archivo. Cuando encuentra un punto, se
## detiene, ya que será la posición del string donde empieza la extensión.
## En ese momento se almacena la posición del punto y guarda True en la
## variable que indica que el archivo tiene extensión. Para tener en cuenta la
## posibilidad de que el archivo no tenga extensión, también se vigila que el
## caracter no sea una barra "/". Si fuese barra, se termina el rastreo de
## caracteres de atrás hacia adelante y se guarda False en la variable que
## indica si el archivo tiene extensión.
    #while i > 0:
        #if archivo_original[i] == ".":
            #tiene_extension = True
            #punto_extension = i
            #break
        #elif archivo_original[i] == "/":
            #tiene_extension = False
            #break
        #else:
            #i = i - 1

## Si el archivo tiene extensión, recorro nuevamente el string, pero en esta
## ocasión de adelante hacia atrás, para capturar los caracteres del nombre del
## archivo pero sin la extensión. Para ello, el recorrido termina cuando se
## haya alcanzado la posición del punto de la extensión, que sabemos del bucle
## anterior. Si el archivo no tiene extensión, pues devolvemos el mismo nombre
## que recibimos.
    #if tiene_extension:
        #i = 0
        #while i < punto_extension:
            #archivo_sin_ext = archivo_sin_ext + archivo_original[i]
            #i = i + 1
    #else:
        #archivo_sin_ext = archivo_original

## Terminamos la función devolviendo True o False para indicar si el archivo
## tiene extensión. Si tiene extensión devuelve el nombre sin la extensión y
## si no la tiene devuelve el nombre tal cual.
    #return(tiene_extension, archivo_sin_ext)


###############################################################################
############ Función para limpiar el nombre de los arhivos de audio ###########
###############################################################################
# Esta función recibe la ruta a un archivo de audio y se queda sólo con el
# nombre del archivo (elimina el resto de la ruta).
# Luego repasa, caracter a caracter si alguno de los caracteres del nombre está
# en la lista /etc/orejon/orejon-nombre.list en cuyo caso cambia el caracter
# por la alternativa que se indique en esa misma lista.
# Por último, recompone la ruta con el nuevo nombre del archivo y lo devuelve.

def limpia_nombre(rutayarchivo):

# Lo primero es eliminar la ruta y quedarse solo con el nombre del archivo.
# Para ello, divido el string con la ruta y el archivo en partes, usando
# la barra "/" como separador y el método "split" del objeto "string". Todas
# estas partes componen una lista. Con el método "pop" del objeto "list", saco
# el último elemento de la lista, que es el nombre del archivo.

    # El método split crea una lista a partir de un string separando por '/'
    partes_rutayarchivo = rutayarchivo.split('/')
    # El método pop saca el último elemento de la lista
    archivo = partes_rutayarchivo.pop()

# Ahora que tengo el nombre del archivo puro, le quito la extensión, para no
# hacer la limpieza de caracteres sobre los caracteres de la extensión.
    (tiene_ext, ext) = extension(archivo)
    (tiene_ext, nombre_sin_ext) = quita_extension(archivo)

# Abro /etc/orejon-nombres.list y leo la tabla de símbolos a eliminar y/o
# reemplazar (lectura línea a línea). Busco cada caracter de la tabla en el
# nombre del archivo de audio y hago uso de los métodos "strip" y "replace"
# del objeto estándar "String" para eliminar o reemplazar dichos caracteres.
    listado_caracteres = open('/etc/orejon/orejon-nombres.list', 'r')
    for linea in listado_caracteres:
        # El archivo /etc/orejon/ojeron-nombres.list admite comentarios,
        # los ignoro
        if linea[0] == "#" or linea == "\n":
            continue
        # Los caracteres pueden no tener reemplazo, en ese caso se eliminan
        elif linea[1] == "\n":
            nombre_sin_ext = nombre_sin_ext.replace(linea[0], "")
        # Si el caracter tiene un reemplazo, se sustituye uno por otro
        else:
            nombre_sin_ext = nombre_sin_ext.replace(linea[0], linea[1])

# Cierro el archivo /etc/orejon-nombres.list
    listado_caracteres.close()

# Una vez acabada la limpieza, añado otra vez la extensión y la ruta. Para
# añadir el archivo modificado a la lista con las partes de la ruta, utilizo
# el método append
    archivo = nombre_sin_ext + ext
    partes_rutayarchivo.append(archivo)

# Reconvierto la lista con las partes de la ruta y el archivo en un string
    rutayarchivo = ''
    for parte in partes_rutayarchivo:
        # Busco la primera parte de la ruta, que será "." o "/" y no meto
        # otra barra más
        if parte == '' or parte == '.':
            rutayarchivo = parte
        # Pero si no es la primera parte de la ruta, separo con "/"
        else:
            rutayarchivo = rutayarchivo + '/' + parte

# Fin de función y devuelvo el archivo modificado
    return(rutayarchivo)


###############################################################################
############# Función para quitar '_orj' al final de los archivos #############
###############################################################################
# Esta función recibe la ruta a un archivo de audio y se queda sólo con el
# nombre del archivo (elimina el resto de la ruta).
# Luego verifica si existe el substring '_orj' (4 caracteres) al final del
# nombre. Si es así, recompone el nombre sin esos últimos 4 caracteres.
# Por último, recompone la ruta con el nuevo nombre del archivo y lo devuelve.

def quita_orj_final(archivo):
# Recibo el nombre del archivo con toda su ruta y la extensión. Separo la
# extensión por un lado y el nombre con la ruta por otro. Uso las funciones
# que hay en este mismo archivo.
    (tiene_ext, ext) = extension(archivo)
    (tiene_ext, nombre_sin_ext) = quita_extension(archivo)

# Ahora determino la longitud del nombre del archivo (más la ruta) y compruebo
# si los últimos cuatro caracteres son "_orj" con el método "find" del objeto
# string. Para cerciorarme que están al final del nombre, acoto la búsqueda
# (find) a los últimos 4 caracteres.
    longitud = len(nombre_sin_ext)
    tiene_orj = nombre_sin_ext.find('_orj', longitud - 4, longitud)

# El método "find" devuelve -1 si no encuentra el texto buscado, por lo que
# pregunto si el resultado del "find" fue -1 o no. Si no lo es, es que los
# últimos 4 caracteres son, efectivamente, "_orj". Procedo a borrarlos sin más
# que recargar el string del nombre del archivo y ruta sin extensión con todo
# el contenido menos los últimos cuatro caracteres [:-4]. Por último,
# recompongo el nombre (modificado) y la extensión del archivo.
    if tiene_orj != -1:
        nombre_sin_ext = nombre_sin_ext[:-4]
        archivo_sin_orj = nombre_sin_ext + ext
    else:
        archivo_sin_orj = nombre_sin_ext + ext

# Termino la función devolviendo el nombre modificado
    return(archivo_sin_orj)